# The house rules:
#  - Define sensitive value everywhere - and think about it!
#  - It's nice to see the types and have a worthwhile description.
#  - das it :)

variable "shared_credentials_file" {
    type = string
    default = "~/.aws/credentials"
    description = "AWS Shared Credentials File. Use alongside aws_profile."
    sensitive = false
}

variable "aws_profile" {
    type = string
    default = "default"
    description = "AWS Shared Credentials Profile. Doesn't do anything for accesskey/secretkey combo."
    sensitive = false
}

variable "AWS_ACCESS_KEY" {
    type = string
    default = ""
    description = "AWS Access Key"
    sensitive = true
}

variable "AWS_SECRET_KEY" {
    type = string
    default = ""
    description = "AWS Secret Key"
    sensitive = true
}
variable "aws_region" {
    type = string
    default = "us-east-1"
    description = "AWS Region to deploy resources by default into."
    sensitive = false
}

variable "ECRRegistryName" {
    type = string
    default = "tfmanagedecr"
    description = "Name of the Elastic Container Repository"
    sensitive = false
}

variable "FrontendDomainName" {
    type = string
    default = "webrtc.dabers.uk"
    description = "Domain name under which the frontend files will exist."
    sensitive = false
}

variable "BackendDomainName" {
    type = string
    default = "server.webrtc.dabers.uk"
    description = "Domain name under which the apprunner app will exist."
    sensitive = false
}

variable "RootDomain" {
    type = string
    default = "dabers.uk"
    description = "Domain name that you actually own in Route53 under which this app will be published - No subdomains!"
    sensitive = false
}