variable "ECR_UserName" {
    type = string
    description = "Registry Authentication Username for AWS ECR."
    sensitive = false
}

variable "ECR_Password" {
    type = string
    description = "Registry Authentication Password for AWS ECR."
    sensitive = true
}

variable "ECR_Url" {
    type = string
    description = "URL for the AWS ECR instance. Includes the image name!"
    sensitive = false
}

variable "ImageTag" {
    type = string
    description = "Image Tag for docker image. Does not include image name."
    default = "1.0"   
    sensitive = false
}

/*
The below variable deserves an explanation.

This application template does a lot of things traditionally wrong - it does deployents via Terraform (will stop that...).
In addition, it does *not* use a load balancer - it saves money on this by instead using DNS as a load balancer. 
This application does not need to be autoscaled.
*/
variable "DomainName" {
    type = string
    description = "Domain name under which this backend will be."
    sensitive = false
}

variable "ZoneId" {
    type = string
    description = "Zone Id (aws_route53_zone.ZONENAME.zone_id)."
    sensitive = false
}