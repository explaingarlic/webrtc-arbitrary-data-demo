output "PreCustomDomainApprunnerDomain" {
    description = "Domain name of the Apprunner service before being assigned to route53 domain."
    value = aws_apprunner_service.application.service_url
    sensitive = false
}