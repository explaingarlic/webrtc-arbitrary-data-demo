terraform {
    required_providers {
        docker = {
            source  = "kreuzwerker/docker"
            version = "2.15.0"
        }
    }
}
provider "docker" {
    host = "unix:///var/run/docker.sock"
    registry_auth {
        address = replace(var.ECR_Url, "//.*/", "")
        username = var.ECR_UserName
        password = var.ECR_Password
    }
}

// Make the Docker image...
resource "docker_registry_image" "appimg"{
    name = "${var.ECR_Url}:${var.ImageTag}"
    
    build {
        context = "${path.root}/../"
        auth_config {
            host_name = replace(var.ECR_Url, "//.*/", "")
            user_name = var.ECR_UserName
            password = var.ECR_Password
        }
    }
}

locals {
    applicationName = "TerraformManagedApp" # TODO: TEMPORARY! 
}

// Have to define this to prevent multiple instances from launching, which would useless due to the stateful nature of the application.
resource "aws_apprunner_auto_scaling_configuration_version" "autoscaling" {
    auto_scaling_configuration_name = "minimal_autoscaling_config"
    max_concurrency = 199
    max_size = 1
    min_size = 1
}

resource "aws_apprunner_service" "application" {
    service_name = "heyheyhey"

    source_configuration {
        image_repository {
            image_configuration {
                port = "8081"
            }
            image_identifier = "${var.ECR_Url}:${var.ImageTag}"
            image_repository_type = "ECR"
        }
      
        authentication_configuration {
            access_role_arn = aws_iam_role.role.arn
        }
    }

    auto_scaling_configuration_arn = aws_apprunner_auto_scaling_configuration_version.autoscaling.arn
}

# Weird way of validating aws apprunner - it's basically an abstraction over ACM, sorta like the
# "aws_acm_certificate_validation". Not sure if it actually does anything or manages a resource.
resource "aws_apprunner_custom_domain_association" "app_domain" {
    domain_name = var.DomainName
    service_arn = aws_apprunner_service.application.arn
}

resource "aws_route53_record" "app_domain" {
    allow_overwrite = true
    name = var.DomainName
    records = [
        aws_apprunner_custom_domain_association.app_domain.dns_target
    ]
    ttl = 60
    type = "CNAME"
    zone_id = var.ZoneId
}

resource "aws_route53_record" "application_validation" {
    for_each = {for r in aws_apprunner_custom_domain_association.app_domain.certificate_validation_records :  r.name => r}
    allow_overwrite = true
    name = each.value.name
    records = [
        each.value.value
    ]
    ttl = 60
    type = each.value.type
    zone_id = var.ZoneId
    depends_on = [
      aws_apprunner_custom_domain_association.app_domain
    ]
}
