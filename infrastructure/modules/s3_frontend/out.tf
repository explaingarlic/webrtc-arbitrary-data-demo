output "BucketARN" {
    description = "AWS Resource Name/Number/Whatever of the AWS Bucket that has the actual frontend data."
    value = aws_s3_bucket.actual_bucket.arn
    sensitive = false
}