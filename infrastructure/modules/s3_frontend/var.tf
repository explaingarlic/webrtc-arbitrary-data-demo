variable "DomainName" {
    type = string
    description = "Domain name under which this backend will be."
    sensitive = false
}

variable "ZoneId" {
    type = string
    description = "Zone Id (aws_route53_zone.ZONENAME.zone_id)."
    sensitive = false
}