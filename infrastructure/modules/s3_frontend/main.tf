# Largely based on:
# https://aws.amazon.com/premiumsupport/knowledge-center/cloudfront-serve-static-website/
# And supplementary articles that help this along.

resource "aws_s3_bucket" "actual_bucket" {
    # Explicit value, rather than using an empty string, for better verbosity.
    bucket = var.DomainName
    acl = "public-read"
    policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
    {
        "Sid": "PublicRead",
        "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${var.DomainName}/*"
        }
    ]
}
EOF

    force_destroy = true # Allow easy destruction, particularly by terraform...

    website {
        index_document = "index.html"
        # These are the same since we use react-router.
        error_document = "index.html"
        # TODO: Decouple this from react-router, possibly with variable? 
    }
    
    # Disable versioning as updates are pushed to the website with the `sync --delete` command/parameter.
    versioning {
        enabled = false
    }
}
/*
resource "aws_s3_bucket_object" "s3object"{
    for_each = fileset("../../web-frontend/build", "*")
    bucket = aws_s3_bucket.actual_bucket.id
    key = each.value
    source = "../../web-frontend/build/${each.value}"
    etag = filemd5("../../web-frontend/build/${each.value}")
}
*/

# TODO: fix this, reformat providers in general...
provider "aws" {
    alias = "use1"
    region = "us-east-1"
}

resource "aws_acm_certificate" "cf_cert"{
    # https://stackoverflow.com/a/69883702/15066285
    provider = aws.use1
    domain_name = var.DomainName
    //subject_alternative_names = local.subject_alternative_names
    validation_method = "DNS"
    
    options {
        # Why is the below a string?
        certificate_transparency_logging_preference = "ENABLED"
    }

    lifecycle {
        create_before_destroy = true # Recommended by terraform docs.
    }
}

resource "aws_route53_record" "certificate_validation_records"{
    for_each = {
        for dnsentry in aws_acm_certificate.cf_cert.domain_validation_options : dnsentry.domain_name => {
        
            name = dnsentry.resource_record_name
            record = dnsentry.resource_record_value
            type = dnsentry.resource_record_type
        }
    }

    allow_overwrite = true
    
    type = each.value.type
    name = each.value.name
    records = [each.value.record]
    zone_id = var.ZoneId
    ttl = 60
}


resource "aws_acm_certificate_validation" "cert" {
  provider = aws.use1
  certificate_arn = aws_acm_certificate.cf_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.certificate_validation_records : record.fqdn]
}

resource "aws_cloudfront_distribution" "site_cf_distribution" {
    origin {
        domain_name = aws_s3_bucket.actual_bucket.bucket_regional_domain_name
        origin_id = "${aws_s3_bucket.actual_bucket.website_domain}-origin"

        # "s3_origin_config" item should only exist if we are using OAI, which we aren't.
        # TODO: Implement OAI so direct access to the bucket is impossible.
        # https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-restricting-access-to-s3.html
    }
    
    enabled = true
    is_ipv6_enabled = true # Not sure if this has a default value. 
    comment = "CF Distribution that allows https ONLY access to website ${var.DomainName}. Redirects all http traffic to https."
    default_root_object = "index.html" # TODO: Make this agnostic of react router.

    # Required for the very last route53_record.
    aliases = ["${var.DomainName}"]

    default_cache_behavior {
        allowed_methods = ["GET", "HEAD"]
        cached_methods = ["GET", "HEAD"]
        target_origin_id = "${aws_s3_bucket.actual_bucket.website_domain}-origin"
        
        # Misleading docs state that the below is optional.
        forwarded_values {
            query_string = false

            cookies {
                forward = "none"
            }
        }

        viewer_protocol_policy = "redirect-to-https"
        min_ttl = 0
        default_ttl = 7200
        max_ttl = 86400
        #TODO: see if forwarded_values is needed.
    }

    

    viewer_certificate {
        acm_certificate_arn = aws_acm_certificate.cf_cert.arn
        ssl_support_method = "sni-only"
    }

    restrictions {
        geo_restriction {
            restriction_type = "blacklist"
            # Ban on chinese traffic. In my experience, lots of spam comes from there, atleast looking at CF logs.
            locations = ["CN"]
        }
    }

    # Lower priority on New Zealand, Australian, Japanese, Indian, South African, Middle Eastern traffic.
    price_class = "PriceClass_200" # Core demographic is in U.S.A, U.K., E.U + Israel. 

    custom_error_response { # TODO: Make this agnostic to react-router.
        error_caching_min_ttl = "60"
        error_code = "404"
        response_code = 200
        response_page_path = "/index.html"
    }

    depends_on = [
      aws_acm_certificate_validation.cert
    ]
}

resource "aws_route53_record" "actual_website_record" {
    zone_id = var.ZoneId
    name = var.DomainName
    type = "A"

    alias { # https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/resource-record-sets-choosing-alias-non-alias.html
        # Remove the trailing "." at the end of the CF domain name.
        name = replace(aws_cloudfront_distribution.site_cf_distribution.domain_name, "/[.]$/", "")
        zone_id = aws_cloudfront_distribution.site_cf_distribution.hosted_zone_id
        evaluate_target_health = false
    }

    # due to function call, this dependency may be needed. (replace)
    depends_on = [aws_cloudfront_distribution.site_cf_distribution]
}