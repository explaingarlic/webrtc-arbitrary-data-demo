output "BackendURL" {
    description = "Backend URL - Ideally, it shouldn't return the view. Use https://{TheDomain}/ping to test."
    value = module.docker_apprunner.PreCustomDomainApprunnerDomain
}
output "FrontendURL" {
    description = "Frontend URL - Should be in an S3 bucket."
    value = "None yet."#TODO
}