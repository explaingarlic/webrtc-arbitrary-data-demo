terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.0"
        }
    }
}

provider "aws" {
    region = var.aws_region
    access_key = var.AWS_ACCESS_KEY != "" ? var.AWS_ACCESS_KEY : null
    secret_key = var.AWS_SECRET_KEY != "" ? var.AWS_SECRET_KEY : null
    shared_credentials_file = var.AWS_ACCESS_KEY == "" ? var.shared_credentials_file : null
    profile = var.aws_profile
}


# ECR Registry...
resource "aws_ecr_repository" "ecr_repo"{
    name = var.ECRRegistryName

    image_tag_mutability = "MUTABLE"

    image_scanning_configuration {
        scan_on_push = true
    }
}

data "aws_ecr_authorization_token" "token" {
    depends_on = [
      aws_ecr_repository.ecr_repo
    ]
}

data "aws_route53_zone" "hostedzone" {
    name = var.RootDomain
    private_zone = false
}

module "docker_apprunner" {
    source = "./modules/docker_apprunner"
    ECR_UserName = data.aws_ecr_authorization_token.token.user_name
    ECR_Password = data.aws_ecr_authorization_token.token.password
    ECR_Url = aws_ecr_repository.ecr_repo.repository_url
    ImageTag = "1.0"
    DomainName = var.BackendDomainName
    ZoneId = data.aws_route53_zone.hostedzone.zone_id
}

module "s3_frontend" {
    source = "./modules/s3_frontend"
    DomainName = var.FrontendDomainName
    ZoneId = data.aws_route53_zone.hostedzone.zone_id
}
/*
module "dockerzipandship" {
    source = "./modules/dockerzipandship"
    Docker_UserPass = data.aws_ecr_authorization_token.token.password
    Docker_UserName = data.aws_ecr_authorization_token.token.user_name
    AWS_ECR_Login_Url = data.aws_ecr_authorization_token.token.proxy_endpoint
    AWS_ECR_Url = aws_ecr_repository.ecr_repo.repository_url
    DockerImage_Name = "applicationbackend:1.0"
    AuthToken = data.aws_ecr_authorization_token.token.authorization_token
}

module "apprunner_app" {
    source = "./modules/apprunnerapp"
    AWS_ECR_Url = data.aws_ecr_authorization_token.token.proxy_endpoint # not sure?
    DockerImage_Name = module.dockerzipandship.DockerImage_Name
    ApplicationName = "Apprunner_Application"
    depends_on = [module.dockerzipandship]
}*/