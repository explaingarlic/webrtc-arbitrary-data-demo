FROM node AS builder
WORKDIR /app
COPY package*.json /app/
COPY src /app/src

RUN npm install --production
FROM alpine

ENV NODE_ENV production
RUN apk add nodejs --no-cache
WORKDIR /app

COPY --from=builder /app/node_modules /app/node_modules
COPY --from=builder /app/src /app/src

EXPOSE 8081
CMD node src/index.js