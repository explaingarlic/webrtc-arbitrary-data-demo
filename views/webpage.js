const inputBoxDom = document.getElementById("peerInputBox");

function addMessage(msg = "empty message..."){
    const msgHistory = document.getElementById("messages");
    let msgContainer = document.createElement("div")
    msgContainer.classList.add("message");
    let msgText = document.createElement("span");
    let msgTime = document.createElement("span");
    msgTime.classList.add("float-end")
    msgTime.innerText = (new Date(Date.now())).toLocaleTimeString()
    msgText.innerText = msg;
    msgContainer.appendChild(msgText);
    msgContainer.appendChild(msgTime);

    msgHistory.insertBefore(msgContainer, inputBoxDom);
};
function flashError(errorText) {
    const errorDom = document.getElementById("error");
    document.getElementById("errorText").textContent = errorText;
    if(errorDom.classList.contains("errorTextComingIn")) {
        errorDom.classList.remove("errorTextComingIn");
        errorDom.classList.add("errorTextComingIn");
    }
    else {
        errorDom.classList.toggle("errorTextComingIn");
    }
}

function StartMessages() {
    inputBoxDom.classList.remove("d-none")
    document.getElementById("messagesPreBlurb").classList.add("d-none")
    document.getElementById("messagesTitle").textContent = "Peer's Messages..."
}

document.getElementById("messageInput").addEventListener("keyup", (e) => {
    if(e.key === "Enter") {
        document.getElementById("messageSubmit").click();
    }
})
document.getElementById("messageSubmit").addEventListener("click", (e) => {
    dataStreams[Object.keys(dataStreams)[0]].send(document.getElementById("messageInput").value);
    
})