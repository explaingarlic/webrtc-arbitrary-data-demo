const ws = new WebSocket("wss://server.webrtc.dabers.uk")//(document.location.href.replace("http://", "ws://"));
let peerConnection = null; // Not const as we want to assign it to null if we want to kill the RTCPeerConnection
let dataStreams = {};

ws.onopen = function(event) {
    ws.onmessage = MessageHandler;
    ws.onclose = function(event) {
        peerConnection = null
        const dsKeys = Object.keys(dataStreams);
        for(let i = 0; i < dsKeys.length; i++) {
            try{
                dataStreams[dsKeys[i]].close();
            }
            catch(e) {
                console.log("Error trying to destroy data stream after disconnect:", e);
            }
        }
        addMessage("Other peer has quit.");
    }
}

function MessageHandler(event) {
    const theMessage = JSON.parse(event.data);
    switch(theMessage.purpose) {
        case "info":
            console.log(theMessage.body.message);
            break;
        case "start":
            if(window.StartMessages) window.StartMessages();
            startRtc(theMessage.body)
            break;
        case "pong":
            addMessage(theMessage.body.message);
            break;
        case "ping":
            addMessage(theMessage.body.message);
            break;
        case "rtc":
            switch(theMessage.body.type)
            {
                case "offer":
                    acceptOffer(theMessage.body.data);
                    break;
                case "newchannel":
                    addRemoteChannel(theMessage.body.id);
                    break;
                case "answer":
                    handleSDPAnswer(theMessage.body.data);
                    break;
                case "ice":
                    addIceCandidate(theMessage.body.data);
                    break;
                default:
                    console.log("Unrecognized rtc request:\n\n",theMessage)
            }
            break;
    }
}
function acceptOffer(theOffer){
    console.log(theOffer)
    peerConnection.setRemoteDescription(theOffer).then(()=>{
        peerConnection.createAnswer().then(answer=>{
            peerConnection.setLocalDescription(answer).then(()=>{
                ws.send(JSON.stringify({
                    purpose: "rtc",
                    body: {
                        type: "answer",
                        data: JSON.stringify(answer)
                    }
                }))
            })
        })
    }).catch(e=>console.log("Error setting remote description:", e));
}

function handleSDPAnswer(answer) {
    
    peerConnection.setRemoteDescription(JSON.parse(answer)).then(e=>{
    }).catch(e=>console.log("Error setting SDP answer:", e));
}
function addRemoteChannel(channelid){
    let dataStream = peerConnection.createDataChannel("Main channel", {
        id: parseInt(channelid),
        ordered: false,
        negotiated: true
    })
    dataStream.onmessage = event => {
        console.log(event)
        addMessage(event.data)
    }
    dataStream.onopen = function(event){
        dataStream.send("This is a test!");
    }
    dataStreams[parseInt(channelid)] = dataStream;
}
function addIceCandidate(candidate){
    peerConnection.addIceCandidate(JSON.parse(candidate)).then(a=>{}).catch(e=>console.log("Error adding ice candidate:",e));
}

function startRtc(rtcMessage){
    peerConnection = new RTCPeerConnection({
        'iceServers': [
            { 'urls': 'stun:stun.l.google.com:19302' },
        ]
    });
    peerConnection.onicecandidate = function(event) {
        if(event.candidate){
            ws.send(JSON.stringify({
                purpose: "rtc",
                body: {
                    type: "ice",
                    data: JSON.stringify(event.candidate)
                }
            }))
        }
        else {
            console.log("All candidates sent and received.")
        }
    }
    addRemoteChannel(101);
    if(rtcMessage.imTheOfferer === true) {// Undefined doesn't work.

        peerConnection.createOffer().then(offer => {
            peerConnection.setLocalDescription(offer).then(a=>{}).catch(e=>console.log("Error setting local description:",e));

            ws.send(JSON.stringify({
                purpose: "rtc",
                body: {
                    type: "offer",
                    data: offer
                }
            }))
        }).catch(e=>console.log("Error creating RTC offer:", e))
    }
    
}

