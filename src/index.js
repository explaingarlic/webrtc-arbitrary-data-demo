/* Library imports... */
const express = require("express");
const crypto = require("crypto");
const hash = (string) => {
    return crypto.createHash("sha256").update(string).digest("hex");
}
const ws = require("ws");
const app = express();
require("dotenv").config();

/* My own code imports... */
const MESSAGES = require("./messages");

function uuid(){
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()  
}

if(process.env.RUNFRONTEND) {
    console.log("Running frontend...")
    app.use(express.static(`${__dirname}/../views`));
    app.get("/", function(req, resp) { resp.sendFile(`${__dirname}/../views/index.html`) })
}


app.get("/ping", function(req, resp) {resp.send("pong"); resp.end()});

const ourPort = process.env.PORT ? process.env.PORT : 8081;
const wsServer = new ws.Server({
    server: app.listen(ourPort, ()=> {
        console.log("We are listening on local port",ourPort);
    })
});


const peopleWaitingForDemo = {};
const connectedFellows = {};

wsServer.on("connection", onconnroutine);

function onconnroutine(socket, thedata){
    socket.id = uuid();
    socket.send(MESSAGES.youArePeer(socket.id));
    socket.peerid = null;
    socket.on("close", ()=> {
        // If this person's not connected, get them out of the queue...
        if(socket.peerid === null) {
            delete peopleWaitingForDemo[socket.id];
            if(connectedFellows[socket.peerid]) {
                connectedFellows[socket.peerid]
            }
        }
        // Else, disconnect the other person safely first.
        else {
            if(connectedFellows[socket.peerid]){
                try {
                    connectedFellows[socket.peerid].send(MESSAGES.otherPersonsGone(socket.id));
                    connectedFellows[socket.peerid].close()
                }
                catch(e){
                    console.log("Attempted to close peer: ", socket, "\n\nPeerid: ", connectedFellows[socket.peerid],"\nconnectedFellows:", Object.keys(connectedFellows), "\nThe error was:",e);
                }
                finally {
                    delete connectedFellows[socket.peerid];
                    delete connectedFellows[socket.id];
                }
            }
        }
    })

    const waitingKeys = Object.keys(peopleWaitingForDemo);
    if(waitingKeys.length){
        // Add already existing person into connectedFellows hashtable...
        connectedFellows[waitingKeys[0]] = peopleWaitingForDemo[waitingKeys[0]];
        delete peopleWaitingForDemo[waitingKeys[0]]
        connectedFellows[waitingKeys[0]].peerid = socket.id;
        
        // Add new guy into connectedFellows hashtable...
        socket.peerid =  connectedFellows[waitingKeys[0]].id;
        connectedFellows[socket.id] = socket;
        
        // Finally: plug in message handler. 
        connectedFellows[waitingKeys[0]].on("message", MessageHandler);
        connectedFellows[socket.id].on("message", MessageHandler);
        
        connectedFellows[socket.id].send(MESSAGES.youAreConnectedMessage(socket.peerid, false))
        connectedFellows[socket.peerid].send(MESSAGES.youAreConnectedMessage(socket.id, true));
    }
    else{
        peopleWaitingForDemo[socket.id] = socket;
    }
}

function MessageHandler(message){
    try {
        var ParsedMsg = JSON.parse(message);
        if (/^\s*$/.test(message)) return;
    }
    catch(e){
        console.log("Error parsing message. Message:", message, "\nError:", e);
        return;
    }
    switch(ParsedMsg.purpose){
        case "ping":
            connectedFellows[this.peerid].send(MESSAGES.pingPongMessage("pong"));
            break;
        case "pong":
            connectedFellows[this.peerid].send(MESSAGES.pingPongMessage("ping"));
            break;
        case "rtc": 
            connectedFellows[this.peerid].send(MESSAGES.rtcMessage(ParsedMsg.body));
            break;
        
        default: 
            this.send(MESSAGES.error("Invalid Message."));
    }
}