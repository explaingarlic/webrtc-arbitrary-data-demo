module.exports = {
    youAreConnectedMessage: (peerid, youAreOfferer) => JSON.stringify({
        purpose: "start",
        body: {
            imTheOfferer: youAreOfferer,
            message: `You are connected to peer ${peerid}`
        }
    }),
    pingPongMessage: (pingOrPong) => JSON.stringify({
        purpose: pingOrPong,
        body : {
            time: Date.now()
        }
    }),
    otherPersonsGone: (peerid) => JSON.stringify({
        purpose: "error",
        body: {
            // != is intentional - it will be null when its unset. If it's undefined, we're 
            // running in an undefined state anyway... Better error out here.
            message: peerid != null && peerid.length ? `${peerid} has disconnected.` : "Peer disconnected - peerid not specified."
        }
    }),
    error: (string) => JSON.stringify({
        purpose: "error",
        body: {
            message: string != null && string.length ? string :  "No error specified."
        }
    }),
    rtcMessage: (msg) => JSON.stringify({
        purpose: "rtc",
        body: msg
    }),
    youArePeer: (msg) => JSON.stringify({
        purpose: "info",
        body: {
            message: `You are peer ${msg}.`
        }
    })
}