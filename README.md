# WebRTC Data Channel POC

Real time, low latency communication *between browsers!!!*

The future is now, folks - Arbitrary UDP connections in web browser.

I recommend using [this resource](https://webrtcforthecurious.com/docs/07-data-communication/)


## What do I do?

`npm start` in root of this repo :)

* Put *.env file in *this* directory, not `src/`.